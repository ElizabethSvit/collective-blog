from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import CharField, PasswordInput, ModelForm

from .models import Post, Comment


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text',)


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('author', 'text',)

class MyRegistrationForm(ModelForm):
    email = CharField(max_length=30, label='Mail')
    first_name = CharField(max_length=30, label='First name')
    last_name = CharField(max_length=30, label='Last name')

    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
        'email_mismatch': "Email addresses is not unique.",
    }
    password1 = CharField(label="Password",
        widget=PasswordInput)
    password2 = CharField(label="Password confirmation",
        widget= PasswordInput,
        help_text="Enter the same password as above, for verification.")

    class Meta:
        model = User
        fields = ['first_name','last_name','password1','password2', 'email']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = email.split('@')[0]
        if email and User.objects.filter(email=email).exists():
            raise ValidationError(
                self.error_messages['email_mismatch'],
                code='email_mismatch')
        return email

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user