from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone

from .models import Post, Comment, Like
from .forms import PostForm, CommentForm, MyRegistrationForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
import json


@login_required
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    user_posts = Post.objects.filter(author=request.user)
    context = {'posts': posts, 'user': request.user, 'user_posts': user_posts}
    return render(request, 'blog/post_list.html', context)


@login_required
def post_my(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    user_posts = Post.objects.filter(author=request.user)
    context = {'posts': posts, 'user': request.user, 'user_posts': user_posts}
    return render(request, 'blog/post_my.html', context)


@login_required
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


@login_required
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})


@login_required
def post_draft_list(request):
    user_posts = Post.objects.filter(published_date__isnull=True, author=request.user).order_by('created_date')
    return render(request, 'blog/post_draft_list.html', {'user_posts': user_posts})


@login_required
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('blog.views.post_detail', pk=pk)


@login_required
def post_remove(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('blog.views.post_list')


@login_required
def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'blog/add_comment_to_post.html', {'form': form})


@login_required
def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('blog.views.post_detail', pk=comment.post.pk)


@login_required
def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    return redirect('blog.views.post_detail', pk=post_pk)


@login_required
def profile(request):
    return render(request, 'blog/profile.html', {})


@login_required
def user_posts(request):
    user_posts = Post.objects.filter(published_date__lte=timezone.now(), author=request.user).order_by('published_date')
    return render(request, 'blog/user_posts.html', {'user_posts': user_posts})


@login_required
def user_friends(request):
    return render(request, 'blog/user_friends.html', {})


@login_required
def find_friends(request):
    return render(request, 'blog/find_friends.html', {})


def registration(request):
    """

    :param request:
    :return:
    """
    if request.method == "POST":
        user_form = MyRegistrationForm(request.POST)  # , instance=task)
        if user_form.is_valid():

            # UserProfile.objects.create_user(username=form.cleaned_data.get('username'),
            # password=form.cleaned_data.get('password'))
            new_email = user_form.clean_email()
            user_object = User.objects.create_user(
                password=user_form.cleaned_data.get('password1'),
                first_name=user_form.cleaned_data.get('first_name'),
                last_name=user_form.cleaned_data.get('last_name'),
                email=new_email,
                username=new_email.split('@')[0]
            )

            user = authenticate(username=user_form.cleaned_data.get('email').split('@')[0],
                                password=user_form.cleaned_data.get('password1')
                                )
            if user is not None:  # add check if exist
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                error_msg = "Please, try again :)"
                return render(request, 'registration/registration.html', {'form': user_form, 'error_msg': error_msg})
    else:
        user_form = MyRegistrationForm()
    return render(request, 'registration/registration.html', {'form': user_form})


@login_required
# AJAX Like request
def like_post(request, post_id):
    method = request.POST.get('method')
    if request.method == 'POST':
        post = get_object_or_404(Post, pk=post_id)
        if method == 'like':
            if Like.objects.filter(post=post, author=request.user):
                raise ValidationError(
                    _('Invalid like: %(value)s'),
                    code='invalid',
                    params={'value': 'already exists'},
                )
            like = Like(author=request.user, post=post)
            like.save()
        elif method == 'dislike':
            like = Like.objects.get(author=request.user, post=post)
            like.delete()

        post.save()
        post.author.save()
        request.user.save()

        response_data = {'post_id': post.pk, 'likes': post.like_set.count()}
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )