__author__ = 'PC'

from django import template
from blog.models import Post, Like
from django.contrib.auth.models import User

register = template.Library()


@register.simple_tag()
def like_post(user, post_id):
    user = User.objects.get(username=user)
    post = Post.objects.get(pk=post_id)
    picture = 'glyphicon glyphicon-thumbs-up'
    opacity = 'disliked'
    method = 'like_button'
    likes = Like.objects.filter(post=post).count()
    if user:
        if post.author == user:
            return '<div class="likepoint" data-id="' + str(post.pk) + '"><span class="post_likes">' + str(
                likes) + '&nbsp;</span><span class="likepic liked ' + picture + '" aria-hidden="true"></span></div>'
        elif Like.objects.filter(author=user, post=post):
            opacity = 'liked'
            method = 'dislike_button'

    return '<div class="likepoint" data-id="' + str(post.pk) + '"><span class="post_likes">' + str(
        likes) + '&nbsp;</span>' + \
           '<a class="liker ' + method + '"><span class="likepic ' + opacity + ' ' + picture + '" aria-hidden="true"></span></a>' + \
           '</div>'
