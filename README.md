# README #

Описание проекта по созданию коллективного блога на языке Python с использованием платформы Django.

### 1. Тема блога ###

Проектом является коллективный блог о путешествиях.

### 2. Описание проекта ###

Это коллективный блог путешествий. Целевая аудитория - семьи, группы друзей или близких, которые любят путешествовать и могут использовать блог в виде дневника для записи своих путешествий. Это хороший способ оставить отличное впечатление от поездки надолго, выложить в одно место множество фотографий, обсуждать и комментировать путешествия с другими пользователями. 

### 3. Функциональность ###

На главной странице находится лента всех записей всех постов людей, на которых подписан данный пользователь. Можно создать посты в виде черновика, затем удалять его или опубликовывать, чтобы видели все. Записи можно комментировать. Комментарии расположены в виде обычной ленты. 
Пользователи и посты имеют систему рейтингов: пользователь может отметить "Мне нравится", а также подписаться на конкретного пользователя и его блог. 
На странице профиля можно просматривать все свои опубликованные посты, смотреть отметки на карте по странам путешествий, просматривать друзей. Также есть страница настройки профиля, там можно менять пароль, вводить и изменять личные данные.

### 4. Технические средства ###

* Фреймворк Django

* База данных SQLite для хранения информации (Модель в Django это объект определенного свойства - он хранится в базе данных)

* QuerySet – список объектов заданной Модели (позволяет читать данные из базы данных, фильтровать и изменять их порядок)  

* Django ORM

* Сервер в интернете с довольно простым процессом публикации: PythonAnywhere

* HTML + CSS + Bootstrap (для разработки красивых сайтов) или/и JavaScript

### 5. План работ ###

Каждый новая доработка проекта будет сдаваться раз в две недели (предположительно в воскресенье или любой другой срок, который возможно будет назначен для всей группы), по указанным контрольным точкам. По ходу двух недель и в процессе работы возможно будут иметься вопросы, которые я буду задавать руководителю или семинаристам по курсу Технологии программирования, или обращаться к [google.com.](Link URL)

### 6. Прочее ###

В папке PROJECT/specs примерный дизайн сайта и UML диаграмма взаимодействия пользователя, других пользователей и админа на уровне создания блога. В папке PROJECT/src код проекта.
Сам сайт можно посетить на [elizaveta.pythonanywhere.com](http://elizaveta.pythonanywhere.com)