from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField
from ckeditor.widgets import CKEditorWidget


class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=10000)
    text = RichTextField(config_name='default')
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        blank=True, null=True)

    def likes_count(self):
        return Like.objects.filter(post__author=self.id).count()

    def save(self, *args, **kwargs):
        # Update likes
        self.likes = self.like_set.count()
        super(Post, self).save(*args, **kwargs)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey('Post', related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

    def approved_comments(self):
        return self.comments.filter(approved_comment=True)


class Like(models.Model):
    author = models.ForeignKey('auth.User')
    post = models.ForeignKey('Post')
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.author.user + ' liked post #' + str(self.post.pk)
